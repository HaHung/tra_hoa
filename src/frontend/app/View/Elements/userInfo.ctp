<div class="col-md-2">
    <div class="navbar navbar-inverse" role="banner">
        <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tài khoản <b class="caret"></b></a>
                    <ul class="dropdown-menu animated fadeInUp">
                        <li><a href="<?php echo Router::url(['controller' => 'Users', 'action' => 'edit']) ?>"><i class="fa fa-wheelchair"></i> Đổi mật khẩu</a></li>
                        <li><a href="<?php echo Router::url(['controller' => 'Secure', 'action' => 'logout']) ?>"><i class="fa fa-sign-out"></i> Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>