<div class="sidebar content-box" style="display: block;">
    <ul class="nav">
        <li class="current"><a href="index.html"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>

        <!-- Người dùng -->
        <li class="submenu">
            <a href="#">
                <i class="fa fa-group"></i>Người dùng
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li><a href="#"><i class="fa fa-list"></i> Danh sách người dùng</a></li>
                <li><a href="#"><i class="fa fa-plus"></i> Thêm người dùng</a></li>
            </ul>
        </li>

        <!-- Sản phẩm -->
        <li class="submenu">
            <a href="#">
                <i class="fa fa-product-hunt"></i>Sản phẩm
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li><a href="#"><i class="fa fa-list"></i> Danh sách sản phẩm</a></li>
                <li><a href="#"><i class="fa fa-plus"></i> Thêm sản phẩm</a></li>
            </ul>
        </li>

        <!--Nhóm sản phẩm -->
        <li class="submenu">
            <a href="#">
                <i class="fa fa-product-hunt"></i>Nhóm sản phẩm
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li><a href="#"><i class="fa fa-list"></i> Danh sách nhóm sản phẩm</a></li>
                <li><a href="#"><i class="fa fa-plus"></i> Thêm nhóm sản phẩm</a></li>
            </ul>
        </li>

        <!--Loại sản phẩm -->
        <li class="submenu">
            <a href="#">
                <i class="fa fa-list"></i>Loại sản phẩm
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li><a href="#"><i class="fa fa-list"></i> Danh sách loại sản phẩm</a></li>
                <li><a href="#"><i class="fa fa-plus"></i> Thêm loại sản phẩm</a></li>
            </ul>
        </li>

        <!-- Bài viết -->
        <li class="submenu">
            <a href="#">
                <i class="fa fa-sticky-note-o"></i>Bài viết
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li><a href="#"><i class="fa fa-list"></i> Danh sách bài viết</a></li>
                <li><a href="#"><i class="fa fa-plus"></i> Thêm bài viết</a></li>
            </ul>
        </li>

        <!-- Đơn hàng -->
        <li class="submenu">
            <a href="#">
                <i class="fa fa-shopping-bag"></i>Đơn hàng
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li><a href="#"><i class="fa fa-list"></i> Danh sách đơn hàng</a></li>
            </ul>
        </li>

        <!-- SlideShow -->
        <li class="submenu">
            <a href="#">
                <i class="fa fa-snowflake-o"></i>SlideShow
                <span class="caret pull-right"></span>
            </a>
            <!-- Sub menu -->
            <ul>
                <li><a href="#"><i class="fa fa-list"></i> Danh sách SlideShow</a></li>
                <li><a href="#"><i class="fa fa-plus"></i> Thêm SlideShow</a></li>
            </ul>
        </li>

    </ul>
</div>