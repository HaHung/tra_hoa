

<div class="content-box-large">

	<div class="panel-heading">
		<h4><legend><?php echo __('Thêm bài viết'); ?></legend></h4>
	</div>

	<div class="panel-body">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
			<tr>
				<th><?php echo $this->Paginator->sort('id'); ?></th>
				<th><?php echo $this->Paginator->sort('user_id'); ?></th>
				<th><?php echo $this->Paginator->sort('title'); ?></th>
				<th><?php echo $this->Paginator->sort('image'); ?></th>
				<!-- <th><?php echo $this->Paginator->sort('content'); ?></th> -->
				<th><?php echo $this->Paginator->sort('status'); ?></th>
				<th><?php echo $this->Paginator->sort('created'); ?></th>
				<th><?php echo $this->Paginator->sort('modified'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($posts as $post): ?>
			<tr class="odd gradeX">
				<td><?php echo h($post['Post']['id']); ?></td>
				<td>
					<?php echo $this->Html->link($post['User']['username'], array('controller' => 'users', 'action' => 'view', $post['User']['id'])); ?>
				</td>
				<td><?php echo h($post['Post']['title']); ?>&nbsp;</td>
				<td><?php echo h($post['Post']['image']); ?>&nbsp;</td>
				<!-- <td><?php echo h($post['Post']['content']); ?>&nbsp;</td> -->
				<td><?php echo h($post['Post']['status']); ?>&nbsp;</td>
				<td><?php echo h($post['Post']['created']); ?>&nbsp;</td>
				<td><?php echo h($post['Post']['modified']); ?>&nbsp;</td>
				<td class="actions">
					<a href="<?php echo $this->Html->url(array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?>"><button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></button></a>
					<a href="<?php echo $this->Html->url(array('controller' => 'posts', 'action' => 'edit', $post['Post']['id'])); ?>"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i></button></a>
					<?php echo $this->Form->postLink($this->Html->tag('i', '', ['class'=>'glyphicon glyphicon-remove']), array('action' => 'delete', $post['Post']['id']), array('escape'=>false,'class'=>'btn btn-danger btn-xs', 'confirm' => __('Are you sure you want to delete # %s?', $post['Post']['id']))); ?>
				</td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

