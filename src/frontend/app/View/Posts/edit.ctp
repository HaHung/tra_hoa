<div class="col-md-12">
    <div class="content-box-large">

        <div class="panel-body">
            <?php echo $this->Form->create('Post'); ?>
            <fieldset>
                <div>
                    <h4>
                        <legend><?php echo __('Sửa bài viết'); ?></legend>
                    </h4>
                <?php echo $this->Form->input('id'); ?>
                <!--
					<p>
						<div class="form-group">
							<label>Username</label>
							<?php echo $this->Form->input('user_id', array('label' => false,

                        'empty' => false,
                        'class' => 'form-control'));
                ?>
                        </div>
                    </p>
                    -->
                </div>
                <div class="form-group">
                    <label>Title</label>
                    <?php echo $this->Form->input('title', array('label' => false,
                            'placeholder' => 'Title',
                            'class' => 'form-control'
                    ));
                    ?>
                </div>
                <div class="form-group">
                    <label>Image</label>
                    <?php echo $this->Form->input('image', array('label' => false,
                            'placeholder' => 'image',
                            'class' => 'form-control'
                    )); ?>

                </div>
                <div class="form-group">
                    <label>Content</label>
                    <?php echo $this->Form->input('content', array('label' => false,
                            'placeholder' => 'Content',
                            'class' => 'form-control',
                            'id' => 'content'
                    )); ?>

                </div>
                <p>
                <div class="form-group">
                    <label>Status</label>
                    <?php echo $this->Form->input('staus', array('label' => false,
                            'options' => array('1' => 'Public', '2' => 'Drafts'),
                            'empty' => false,
                            'class' => 'form-control'));
                    ?>
                </div>
                </p>
            </fieldset>
            <div>
                <?php echo $this->Form->end(array(
                        'label' => 'Edit',
                        'class' => 'btn btn-primary'
                )); ?>
            </div>

        </div>
    </div>
</div>

<?php
$this->start('scripts');
?>
<script>
    $(function () {
        $('textarea#content').ckeditor({width: '98%', height: '150px'});
    });
</script>
<?php
$this->end();