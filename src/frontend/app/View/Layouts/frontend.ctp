<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Thảo Trà Shop</title>
    <?php echo $this->Html->meta("/images/logo.gif", "/images/logo.gif", array('type' => 'icon')); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <?php
        echo $this->Html->css([
            '/js/bxslider/jquery.bxslider.css',
            '/js/flexslider/flexslider.css',
            '/js/radial-progress/style.css',
            '/css/animate.css',
            '/css/bootstrap.min.css',
            '/js/bootstrap-progressbar/bootstrap-progressbar-3.2.0.min.css',
            '/js/jqueryui/jquery-ui.css',
            '/js/jqueryui/jquery-ui.structure.css',
            '/js/fancybox/jquery.fancybox.css',
            '/fonts/fonts.css',
            '/css/main-default.css',
            '/js/rs-plugin/css/settings.css',
            '/js/rs-plugin/css/settings-custom.css',
            '/js/rs-plugin/videojs/video-js.css',
            'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext',
            'http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic',
            'http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css',
        ]);
    ?>
</head>
<body>
<div class="mask-l" style="background-color: #fff; width: 100%; height: 100%; position: fixed; top: 0; left:0; z-index: 9999999;"></div> <!--removed by integration-->

<header>
    <!--thông tin liên hệ và giỏ hàng-->
    <div class="b-top-options-panel b-top-options-panel--color">
        <div class="container">
            <!--thông tin liên hệ-->
            <div class="b-option-contacts f-option-contacts">
                <a href="mailto:frexystudio@gmail.com"><i class="fa fa-envelope-o"></i> dbn237@gmail.com</a>
                <a href="#"><i class="fa fa-phone"></i> +84 905 270 495 </a>
                <a href="#"><i class="fa fa-skype"></i> ask.company</a>
            </div>
            <!-- end thông tin liên hệ-->
            <!--giỏ hàng-->
            <div class="b-option-total-cart">
                <div class="b-option-total-cart__goods">
                    <a href="#" class="f-option-total-cart__numbers b-option-total-cart__numbers"><i class="fa fa-shopping-cart"></i> Cart [ 0 ] item</a>
                    <div class="b-option-cart__items">
                        <div class="b-option-cart__items__title f-option-cart__items__title f-default-l">Recently added item(s)</div>
                        <ul>
                            <li>
                                <div class="b-option-cart__items__img">
                                    <div class="view view-sixth">
                                        <img data-retina="" src="img/shop/cart_mini_pic.jpg" alt="">
                                        <div class="b-item-hover-action f-center mask">
                                            <div class="b-item-hover-action__inner">
                                                <div class="b-item-hover-action__inner-btn_group">
                                                    <a href="#" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-option-cart__items__descr">
                                    <strong class="b-option-cart__descr__title f-option-cart__descr__title"><a href="#">Product Example</a></strong>
                                    <span class="b-option-cart__descr__cost f-option-cart__descr__cost">1 x $239</span>
                                </div>
                                <i class="fa fa-times b-icon--fa"></i>
                            </li>
                            <li>
                                <div class="b-option-cart__items__img">
                                    <div class="view view-sixth">
                                        <img data-retina="" src="img/shop/cart_mini_pic.jpg" alt="">
                                        <div class="b-item-hover-action f-center mask">
                                            <div class="b-item-hover-action__inner">
                                                <div class="b-item-hover-action__inner-btn_group">
                                                    <a href="#" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-option-cart__items__descr">
                                    <strong class="b-option-cart__descr__title f-default-b f-option-cart__descr__title"><a href="#">Product Example</a></strong>
                                    <span class="b-option-cart__descr__cost f-option-cart__descr__cost">1 x $239</span>
                                </div>
                                <i class="fa fa-times b-icon--fa"></i>
                            </li>
                        </ul>
                        <div class="b-option-cart__btn">
                            <button class="button-xs button-gray-light">Cancel</button>
                            <button class="button-xs">Check out  </button>
                        </div>
                    </div>
                </div>
            </div>
            <!--end giỏ hàng-->
        </div>
    </div>
    <!--end thông tin liên hệ và giỏ hàng-->
    <!-- Menu có cả logo -->
    <div class="container b-header__box b-relative">
        <!-- LOGO -->
        <a href="/" class="b-left b-logo"><img class="color-theme" data-retina src="img/logo-header-default.png" alt="Logo" /></a>

        <div class="b-header-r b-right">
            <!-- Nút tìm kiếm -->
            <div class="b-header-ico-group f-header-ico-group b-right">
                          <span class="b-search-box">
                              <i class="fa fa-search"></i>
                              <input type="text" placeholder="Enter your keywords"/>
                          </span>
            </div>
            <!-- end Nút tìm kiếm -->
            <div class="b-top-nav-show-slide f-top-nav-show-slide b-right j-top-nav-show-slide"><i class="fa fa-align-justify"></i></div>
            <nav class="b-top-nav f-top-nav b-right j-top-nav">
                <ul class="b-top-nav__1level_wrap">
                    <!-- Menu -->
                    <li class="b-top-nav__1level f-top-nav__1level is-active-top-nav__1level f-primary-b"><a href="index.html"><i class="fa fa-home b-menu-1level-ico"></i>Home <span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a></li>
                    <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                        <a href="blog_listing.html"><i class="fa fa-code b-menu-1level-ico"></i>Blog <span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>

                    </li>

                    <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                        <a href="shop_listing_col.html"><i class="fa fa-list b-menu-1level-ico"></i>Shop <span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>
                        <div class="b-top-nav__dropdomn">
                            <ul class="b-top-nav__2level_wrap">
                                <li class="b-top-nav__2level_title f-top-nav__2level_title">Shopping</li>
                                <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="shop_listing_col.html"><i class="fa fa-angle-right"></i>Trà an thần</a></li>
                                <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="shop_listing_col.html"><i class="fa fa-angle-right"></i>Thảo dược tăng vị</a></li>
                                <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="shop_listing_col.html"><i class="fa fa-angle-right"></i>Trà giảm cân làm đẹp</a></li>
                                <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="shop_listing_col.html"><i class="fa fa-angle-right"></i>Trà tăng cường sức khỏe</a></li>
                                <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="shop_listing_col.html"><i class="fa fa-angle-right"></i>Quà tặng</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                        <a href="contact_us.html"><i class="fa fa-folder-open b-menu-1level-ico"></i>Contact us<span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>
                        <!--<div class="b-top-nav__dropdomn">-->
                            <!--<ul class="b-top-nav__2level_wrap">-->
                                <!--<li class="b-top-nav__2level_title f-top-nav__2level_title">Contact us</li>-->
                                <!--<li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="contact_us.html"><i class="fa fa-angle-right"></i>Version1</a></li>-->
                                <!--<li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="contact_us_v2.html"><i class="fa fa-angle-right"></i>Version2</a></li>-->
                            <!--</ul>-->
                        <!--</div>-->
                    </li>



                    <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                        <a href="/users/login"><i class="fa fa-sign-in b-menu-1level-ico"></i>Đăng nhập<span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>
                    </li>

                    <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                        <a href="/users/add"><i class="fa fa-arrow-up b-menu-1level-ico"></i>Đăng kí<span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>
                    </li>

                    <!--Hết Menu-->
                </ul>

            </nav>
        </div>
    </div>
    <!-- hết Menu có cả logo -->
</header>
<div class="j-menu-container">

</div>


<div class="l-main-container">
    <!-- content here -->
    <?php echo $this->fetch('content')?>
</div>
        <!-- bắt đầu footer -->
<footer>
        <!-- thanh ngang dưới phần footer -->
    <div class="b-footer-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-xs-12 f-copyright b-copyright">Copyright © 2016 - All Rights Reserved</div>
                <div class="col-sm-8 col-xs-12">
                    <div class="b-btn f-btn b-btn-default b-right b-footer__btn_up f-footer__btn_up j-footer__btn_up">
                        <i class="fa fa-chevron-up"></i>
                    </div>
                    <nav class="b-bottom-nav f-bottom-nav b-right hidden-xs">
                        <ul>
                            <li><a href="index.html">Homepage</a></li>
                            <li><a href="blog_listing.html">Blog</a></li>
                            <li><a href="shop_listing_col.html">Shop</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!-- kết thúc  -->

    <div class="container">
        <div class="b-footer-secondary row">
        <!-- logo -->
            <div class="col-md-3 col-sm-12 col-xs-12 f-center b-footer-logo-containter">
                <a href=""><img data-retina class="b-footer-logo color-theme" src="img/logo-vertical-default.jpg" alt="Logo"/></a>
                <div class="b-footer-logo-text f-footer-logo-text">
                    <p>Mauris rhoncus pretium porttitor. Cras scelerisque commodo odio.</p>
                    <div class="b-btn-group-hor f-btn-group-hor">
                        <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
                            <i class="fa fa-dribbble"></i>
                        </a>
                        <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
                            <i class="fa fa-behance"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!-- kết thúc -->
            <!-- bài viết mới nhất -->
            <div class="col-md-3 col-sm-12 col-xs-12">
                <h4 class="f-primary-b">Latest blog posts</h4>
                <div class="b-blog-short-post row">
                    <div class="b-blog-short-post__item col-md-12 col-sm-4 col-xs-12 f-primary-b">
                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                            <a href="blog_detail.html">Amazing post with all the goodies</a>
                        </div>
                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date">
                            March 23, 2013
                        </div>
                    </div>
                    <div class="b-blog-short-post__item col-md-12 col-sm-4 col-xs-12 f-primary-b">
                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                            <a href="blog_detail.html">Amazing post with all the goodies</a>
                        </div>
                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date">
                            March 23, 2013
                        </div>
                    </div>
                    <div class="b-blog-short-post__item col-md-12 col-sm-4 col-xs-12 f-primary-b">
                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                            <a href="blog_detail.html">Amazing post with all the goodies</a>
                        </div>
                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date">
                            March 23, 2013
                        </div>
                    </div>
                </div>
            </div>
            <!-- kết thúc ô bài viết -->
            <!-- contact -->
            <div class="col-md-3 col-sm-12 col-xs-12">
                <h4 class="f-primary-b">contact info</h4>
                <div class="b-contacts-short-item-group">
                    <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
                        <div class="b-contacts-short-item__icon f-contacts-short-item__icon f-contacts-short-item__icon_lg b-left">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="b-remaining f-contacts-short-item__text">
                            Frexy Studio<br/>
                            1234 Street Name, City Name,<br/>
                            United States.<br/>
                        </div>
                    </div>
                    <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
                        <div class="b-contacts-short-item__icon f-contacts-short-item__icon b-left f-contacts-short-item__icon_md">
                            <i class="fa fa-skype"></i>
                        </div>
                        <div class="b-remaining f-contacts-short-item__text f-contacts-short-item__text_phone">
                            Skype: ask.company
                        </div>
                    </div>
                    <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
                        <div class="b-contacts-short-item__icon f-contacts-short-item__icon b-left f-contacts-short-item__icon_xs">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <div class="b-remaining f-contacts-short-item__text f-contacts-short-item__text_email">
                            <a href="mailto:dbn237@gmail.com">dbn237@gmail.com</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- kết thúc -->
            <!-- photo -->
            <div class="col-md-3 col-sm-12 col-xs-12 ">
                <h4 class="f-primary-b">photo stream</h4>
                <div class="b-short-photo-items-group">
                    <div class="b-column">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_1.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_1.jpg" alt=""/></a>
                    </div>
                    <div class="b-column">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_2.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_2.jpg" alt=""/></a>
                    </div>
                    <div class="b-column">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_3.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_3.jpg" alt=""/></a>
                    </div>
                    <div class="b-column">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_4.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_4.jpg" alt=""/></a>
                    </div>
                    <div class="b-column">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_5.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_5.jpg" alt=""/></a>
                    </div>
                    <div class="b-column">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_6.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_6.jpg" alt=""/></a>
                    </div>
                    <div class="b-column">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_7.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_7.jpg" alt=""/></a>
                    </div>
                    <div class="b-column">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_8.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_8.jpg" alt=""/></a>
                    </div>
                    <div class="b-column hidden-xs">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_9.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_9.jpg" alt=""/></a>
                    </div>
                    <div class="b-column hidden-sm hidden-xs">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_10.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_10.jpg" alt=""/></a>
                    </div>
                    <div class="b-column hidden-sm hidden-xs">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_11.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_11.jpg" alt=""/></a>
                    </div>
                    <div class="b-column hidden-sm hidden-xs">
                        <a class="b-short-photo-item fancybox" href="img/gallery/big/gallery_12.jpg" title="photo stream" rel="footer-group"><img width="62" height="62" data-retina src="img/gallery/sm/gallery_12.jpg" alt=""/></a>
                    </div>
                </div>
            </div>
            <!-- kết thúc -->
        </div>
    </div>
</footer>
<?php
    echo $this->Html->script([
        '/js/breakpoints.js',
        '/js/jquery/jquery-1.11.1.min.js',
        '/js/scrollspy.js',
        '/js/bootstrap-progressbar/bootstrap-progressbar.js',
        '/js/bootstrap.min.js',
        '/js/masonry.pkgd.min.js',
        '/js/imagesloaded.pkgd.min.js',
        '/js/bxslider/jquery.bxslider.min.js',
        '/js/flexslider/jquery.flexslider.js',
        '/js/smooth-scroll/SmoothScroll.js',
        '/js/jquery.carouFredSel-6.2.1-packed.js',
        '/js/rs-plugin/js/jquery.themepunch.plugins.min.js',
        '/js/rs-plugin/js/jquery.themepunch.revolution.min.js',
        '/js/rs-plugin/videojs/video.js',
        '/js/jqueryui/jquery-ui.js',
        'http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCfVS1-Dv9bQNOIXsQhTSvj7jaDX7Oocvs',
        '/js/modules/sliders.js',
        '/js/modules/ui.js',
        '/js/modules/retina.js',
        '/js/modules/animate-numbers.js',
        '/js/modules/parallax-effect.js',
        '/js/modules/settings.js',
        '/js/modules/maps-google.js',
        '/js/modules/color-themes.js',
        '/js/audioplayer/js/jquery.jplayer.min.js',
        '/js/audioplayer/js/jplayer.playlist.min.js',
        '/js/audioplayer.js',
        '/js/radial-progress/jquery.easing.1.3.js',
        'http://cdnjs.cloudflare.com/ajax/libs/d3/3.4.13/d3.min.js',
        '/js/radial-progress/radialProgress.js',
        '/js/progressbars.js',
        'https://www.google.com/jsapi?autoload={\'modules\':[{\'name\':\'visualization\',\'version\':\'1\',\'packages\':[\'corechart\']}]}',
        '/js/google-chart.js',
        '/js/j.placeholder.js',
        '/js/fancybox/jquery.fancybox.pack.js',
        '/js/fancybox/jquery.mousewheel.pack.js',
        '/js/fancybox/jquery.fancybox.custom.js',
        '/js/user.js',
        '/js/timeline.js',
        '/js/fontawesome-markers.js',
        '/js/markerwithlabel.js',
        '/js/cookie.js',
        '/js/loader.js',
        '/js/scrollIt/scrollIt.min.js',
        '/js/modules/navigation-slide.js',

    ]);
?>
<!--<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>-->
</body>
</html>