
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->Html->css(['http://fonts.googleapis.com/css?family=Oxygen:400,300,700', 'register']); ?>
    <!--webfonts-->

</head>
<body>
<div class="container">
    <?php echo $this->fetch('content'); ?>
</div>
<!-----//end-copyright---->
</body>
</html>