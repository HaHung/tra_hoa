<div class="flowers index">
	<h2><?php echo __('Flowers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('category_id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('slug'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('info'); ?></th>
			<th><?php echo $this->Paginator->sort('price'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_price'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_off'); ?></th>
			<th><?php echo $this->Paginator->sort('published'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($flowers as $flower): ?>
	<tr>
		<td><?php echo h($flower['Flower']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($flower['Category']['name'], array('controller' => 'categories', 'action' => 'view', $flower['Category']['id'])); ?>
		</td>
		<td><?php echo h($flower['Flower']['title']); ?>&nbsp;</td>
		<td><?php echo h($flower['Flower']['slug']); ?>&nbsp;</td>
		<td><?php echo h($flower['Flower']['image']); ?>&nbsp;</td>
		<td><?php echo h($flower['Flower']['info']); ?>&nbsp;</td>
		<td><?php echo h($flower['Flower']['price']); ?>&nbsp;</td>
		<td><?php echo h($flower['Flower']['sale_price']); ?>&nbsp;</td>
		<td><?php echo h($flower['Flower']['sale_off']); ?>&nbsp;</td>
		<td><?php echo h($flower['Flower']['published']); ?>&nbsp;</td>
		<td><?php echo h($flower['Flower']['created']); ?>&nbsp;</td>
		<td><?php echo h($flower['Flower']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $flower['Flower']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $flower['Flower']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $flower['Flower']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $flower['Flower']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Flower'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Comments'), array('controller' => 'comments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Comment'), array('controller' => 'comments', 'action' => 'add')); ?> </li>
	</ul>
</div>
