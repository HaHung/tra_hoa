

<div class="col-md-6">
	<div class="content-box-large">

		<div class="panel-body">
			<?php echo $this->Form->create('User'); ?>
			<fieldset>
				<div>
					<h4><legend><?php echo __('Sửa người dùng'); ?></legend></h4>
					<?php echo $this->Form->input('id'); ?>
					<p>
					<div class="form-group">
						<label>Group</label>
						<?php echo $this->Form->input('group_id', array('label' => false,
								'options' => array('1'=>'Admin', '2'=>'User'),
								'empty' => false,
								'class' => 'form-control' ));
						?>
					</div>
					</p>
				</div>
				<div class="form-group">
					<label>Username</label>
					<?php echo $this->Form->input('username', array('label' => false,
							'disabled' => true,
							'placeholder' => 'Username',
							'class' => 'form-control'
					));
					?>
				</div>
				<div class="form-group">
					<label>Password</label>
					<?php echo $this->Form->input('password', array('label' => false,
							'placeholder' => 'Password',
							'value' => 'mypassword',
							'class' => 'form-control'
					));
					?>
				</div>
				<div class="form-group">
					<label>Email</label>
					<?php echo $this->Form->input('email', array('label' => false,
							'placeholder' => 'Email',
							'class' => 'form-control'
					)); ?>

				</div>
				<div class="form-group">
					<label>Firstname</label>
					<?php echo $this->Form->input('firstname', array('label' => false,
							'placeholder' => 'Firstname',
							'class' => 'form-control'
					)); ?>

				</div>
				<div class="form-group">
					<label>Lastname</label>
					<?php echo $this->Form->input('lastname', array('label' => false,
							'placeholder' => 'Lastname',
							'class' => 'form-control'
					)); ?>

				</div>
				<div class="form-group">
					<label>Address</label>
					<?php echo $this->Form->input('address', array('label' => false,
							'placeholder' => 'Address',
							'class' => 'form-control'
					)); ?>

				</div>
				<div class="form-group">
					<label>Phone number</label>
					<?php echo $this->Form->input('phone_number', array('label' => false,
							'placeholder' => 'Phone number',
							'class' => 'form-control'
					)); ?>

				</div>
			</fieldset>
			<div>
				<?php echo $this->Form->end(array(
						'label' => 'Edit',
						'class' => 'btn btn-primary'
				)); ?>
			</div>

		</div>
	</div>
</div>