

        <div class="panel-body">

            <fieldset>
                <div>
                    <h2><span class="label label-danger">Đăng kí</span></h2>
                    <?php echo $this->Form->create('User'); ?>
                    <p>
                    <div class="form-group">
                        <?php echo $this->Form->input('group_id', array('label' => false,
                            'value' => 2,
                            'type' => 'hidden'
                            ));
                        ?>
                    </div>
                    </p>
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <?php echo $this->Form->input('username', array('label' => false,
                        'placeholder' => 'Username',
                        'class' => 'form-control'
                    ));
                    ?>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <?php echo $this->Form->input('password', array('label' => false,
                        'placeholder' => 'Password',
                        'class' => 'form-control'
                    ));
                    ?>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <?php echo $this->Form->input('email', array('label' => false,
                        'placeholder' => 'Email',
                        'class' => 'form-control'
                    )); ?>

                </div>
                <div class="form-group">
                    <label>Tên</label>
                    <?php echo $this->Form->input('firstname', array('label' => false,
                        'placeholder' => 'Firstname',
                        'class' => 'form-control'
                    )); ?>

                </div>
                <div class="form-group">
                    <label>Họ</label>
                    <?php echo $this->Form->input('lastname', array('label' => false,
                        'placeholder' => 'Lastname',
                        'class' => 'form-control'
                    )); ?>

                </div>
                <div class="form-group">
                    <label>Địa chỉ</label>
                    <?php echo $this->Form->input('address', array('label' => false,
                        'placeholder' => 'Address',
                        'class' => 'form-control'
                    )); ?>

                </div>
                <div class="form-group">
                    <label>Số điện thoại</label>
                    <?php echo $this->Form->input('phone_number', array('label' => false,
                        'placeholder' => 'Phone number',
                        'class' => 'form-control'
                    )); ?>

                </div>
            </fieldset>
            <div>
                <?php echo $this->Form->end(array(
                    'label' => 'Đăng kí',
                    'class' => 'btn btn-danger'
                )); ?>
            </div>

        </div>



<!--<div class="main">-->
<!--    <div class="social-icons">-->
<!--        <div class="col_1_of_f span_1_of_f"><a href="#">-->
<!--                <ul class='facebook'>-->
<!--                    <i class="fb"> </i>-->
<!--                    <li>Đăng kí bằng Facebook</li>-->
<!--                    <div class='clear'> </div>-->
<!--                </ul>-->
<!--            </a>-->
<!--        </div>-->
<!--        <div class="col_1_of_f span_1_of_f"><a href="#">-->
<!--                <ul class='twitter'>-->
<!--                    <i class="tw"> </i>-->
<!--                    <li>Đăng kí bằng Twitter</li>-->
<!--                    <div class='clear'> </div>-->
<!--                </ul>-->
<!--            </a>-->
<!--        </div>-->
<!--        <div class="clear"> </div>-->
<!--    </div>-->
<!--    <h2>Hoặc đăng kí ngay:</h2>-->
<!--    <form>-->
<!---->
<!--        <div class="lable">-->
<!--            <div class="col_1_of_2 span_1_of_2">	<input type="text" class="text" value="First Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'First Name';}" id="active"></div>-->
<!--            <div class="col_1_of_2 span_1_of_2"><input type="text" class="text" value="Last Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Last Name';}"></div>-->
<!--            <div class="clear"> </div>-->
<!--        </div>-->
<!--        <div class="lable-2">-->
<!--            <input type="text" class="text" value="your@email.com " onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'your@email.com ';}">-->
<!--            <input type="password" class="text" value="Password " onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password ';}">-->
<!--        </div>-->
<!---->
<!--        <h3>Với việc tạo tài khoản, bạn sẽ đồng ý với <span class="term"><a href="#">Điều khoản hoạt động</a></span></h3>-->
<!--        <div class="submit">-->
<!--            <input type="submit" onclick="myFunction()" value="Create account" >-->
<!--        </div>-->
<!--        <div class="clear"> </div>-->
<!--    </form>-->
<!--    <!-----//end-main---->
<!--</div>-->
<!--<!-----start-copyright---->
<!--<div class="copy-right">-->
<!--    DUCNT27-->
<!--</div>-->