<!---->
<!--  Created by PhpStorm.-->
<!--  User: trungducng-->
<!--  Date: 1/14/2017-->
<!--  Time: 5:46 AM-->

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-lock"></span> Login</div>
                <div class="panel-body">
                        <?php echo $this->Form->create('User', array(
                                    'class' => 'form-horizontal',
                                    'role' => 'form'
                        )); ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">
                                Username</label>
                            <div class="col-sm-9">
                                <?php echo $this->Form->input('username', array('label'=>false,'class'=>'form-control','type'=>'text', 'placeholder'=>'Username', 'required' => 'required')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">
                                Password</label>
                            <div class="col-sm-9">
                                <?php echo $this->Form->input('password', array('label'=>false,'class'=>'form-control','type'=>'password', 'placeholder'=>'Password', 'required' => 'required')); ?>
                            </div>
                        </div>

                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">

                                <?php echo $this->Form->input('Login', array('label'=>false,'class'=>'btn btn-success btn-sm','type'=>'submit', 'placeholder'=>'Password')); ?>
                            </div>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="panel-footer">
                    Not Registred? <a href="users/add">Register here</a></div>
            </div>
        </div>
    </div>
</div>
