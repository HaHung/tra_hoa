<?php
App::uses('AppController', 'Controller');
/**
 * Flowers Controller
 *
 * @property Flower $Flower
 * @property PaginatorComponent $Paginator
 */
class FlowersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	public $layout = 'frontend';

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Flower->recursive = 0;
		$this->set('flowers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Flower->exists($id)) {
			throw new NotFoundException(__('Invalid flower'));
		}
		$options = array('conditions' => array('Flower.' . $this->Flower->primaryKey => $id));
		$this->set('flower', $this->Flower->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Flower->create();
			if ($this->Flower->save($this->request->data)) {
				$this->Flash->success(__('The flower has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The flower could not be saved. Please, try again.'));
			}
		}
		$categories = $this->Flower->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Flower->exists($id)) {
			throw new NotFoundException(__('Invalid flower'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Flower->save($this->request->data)) {
				$this->Flash->success(__('The flower has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The flower could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Flower.' . $this->Flower->primaryKey => $id));
			$this->request->data = $this->Flower->find('first', $options);
		}
		$categories = $this->Flower->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Flower->id = $id;
		if (!$this->Flower->exists()) {
			throw new NotFoundException(__('Invalid flower'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Flower->delete()) {
			$this->Flash->success(__('The flower has been deleted.'));
		} else {
			$this->Flash->error(__('The flower could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('index', 'view', 'search');
    }

    public function afterFilter() {
        parent::afterFilter();
        // Allow users to register and logout.
        $this->Auth->allow('index', 'view', 'addToCart');
    }

    public function search(){
	    if($this->request->is('post')){
            $a=$this->request->data('title');
	        $data=$this->Flower->find('all', array(
	            'conditions' => array('title like' => "%$a%"),
	        ));
	        $this->set('data', $data);
        }
    }
}
