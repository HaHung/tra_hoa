<?php
App::uses('AppController', 'Controller');
/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 12/5/2016
 * Time: 6:55 PM
 */
class DefaultController extends AppController
{
    public $layout='frontend';
    public function index() {

    }

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('index', 'view');
    }

    public function afterFilter() {
        parent::afterFilter();
        // Allow users to register and logout.
        $this->Auth->allow('index', 'view');
    }


}