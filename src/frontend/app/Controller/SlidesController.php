<?php
App::uses('AppController', 'Controller');
/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 12/5/2016
 * Time: 7:46 PM
 */
class SlidesController extends AppController
{
    public function index() {

    }

    public function add() {

    }

    public function edit() {

    }

    public function delete() {

    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index', 'view');
    }

    public function afterFilter() {
        parent::afterFilter();
        $this->Auth->allow('index', 'view');
    }
}
