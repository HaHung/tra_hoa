<?php
App::uses('AppController', 'Controller');
/**
 * Slideshows Controller
 *
 * @property Slideshow $Slideshow
 * @property PaginatorComponent $Paginator
 */
class SlideshowsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Slideshow->recursive = 0;
		$this->set('slideshows', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Slideshow->exists($id)) {
			throw new NotFoundException(__('Invalid slideshow'));
		}
		$options = array('conditions' => array('Slideshow.' . $this->Slideshow->primaryKey => $id));
		$this->set('slideshow', $this->Slideshow->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Slideshow->create();
			if ($this->Slideshow->save($this->request->data)) {
				$this->Flash->success(__('The slideshow has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The slideshow could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Slideshow->exists($id)) {
			throw new NotFoundException(__('Invalid slideshow'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Slideshow->save($this->request->data)) {
				$this->Flash->success(__('The slideshow has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The slideshow could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Slideshow.' . $this->Slideshow->primaryKey => $id));
			$this->request->data = $this->Slideshow->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Slideshow->id = $id;
		if (!$this->Slideshow->exists()) {
			throw new NotFoundException(__('Invalid slideshow'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Slideshow->delete()) {
			$this->Flash->success(__('The slideshow has been deleted.'));
		} else {
			$this->Flash->error(__('The slideshow could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index', 'view');
    }

    public function afterFilter() {
        parent::afterFilter();
        $this->Auth->allow('index', 'view');
    }
}
