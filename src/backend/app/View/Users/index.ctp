<div class="content-box-large">

    <div class="panel-heading">
        <h1><span class="label label-danger">Danh sách người dùng</span></h1>
    </div>

	<div class="panel-body">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
			<tr>
				<th>Id</th>
				<th>Group id</th>
				<th>Username</th>
				<th>Email</th>
				<th>Firstname</th>
				<th>Lastname</th>
				<th>Address</th>
				<th>Phone number</th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user): ?>
			<tr class="odd gradeX">
				<td><?php echo h($user['User']['id']); ?></td>
				<td><?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?></td>
				<td><?php echo h($user['User']['username']); ?></td>
				<td><?php echo h($user['User']['email']); ?></td>
				<td><?php echo h($user['User']['firstname']); ?></td>
				<td><?php echo h($user['User']['lastname']); ?></td>
				<td><?php echo h($user['User']['address']); ?></td>
				<td><?php echo h($user['User']['phone_number']); ?></td>
				<td class="actions">
					<a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'view', $user['User']['id'])); ?>"><button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></button></a>
					<a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?>"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i></button></a>
					<?php echo $this->Form->postLink($this->Html->tag('i', '', ['class'=>'glyphicon glyphicon-remove']), array('action' => 'delete', $user['User']['id']), array('escape'=>false,'class'=>'btn btn-danger btn-xs', 'confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?>
				</td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>


















