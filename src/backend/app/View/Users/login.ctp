 <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-wrapper">
                <div class="box">
                    <div class="content-wrap">
                        <h6>Đăng nhập</h6>
                        <div class="social">
                            <!--                            <i style="float: left" class="fa fa-facebook fa-5x"></i>-->
                            <!--                            <i class="fa fa-facebook fa-5x"></i>-->
                            <!--                            <i style="float: right" class="fa fa-facebook fa-5x"></i>-->
                            <div class="division">
                                <hr class="left">
                                <span>***</span>
                                <hr class="right">
                            </div>
                        </div>
                        <?php echo $this->Form->create('User'); ?>
                        <?php echo $this->Form->input('username', array('label'=>false,'class'=>'form-control','type'=>'text', 'placeholder'=>'Username',)); ?>
                        <?php echo $this->Form->input('password', array('label'=>false,'class'=>'form-control','type'=>'password', 'placeholder'=>'Password')); ?>

                        <div class="action">
                            <?php echo $this->Form->input('Login', array('label'=>false,'class'=>'btn btn-primary signup','type'=>'submit', 'placeholder'=>'Password')); ?>

                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>