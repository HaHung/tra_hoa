
<div class="content-box-large">

	<div class="panel-heading">
		<h4><legend><?php echo __('Danh mục sản phẩm'); ?></legend></h4>
	</div>

	<div class="panel-body">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
			<tr>
				<th>STT</th>
				<th>Tên</th>
				<th>Slug</th>
				<th>Miêu tả</th>
				<th>Ngày tạo</th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php $i = 1; ?>
			<?php foreach ($categories as $category): ?>
			<tr class="odd gradeX">
				<td><?php echo $i++; ?>&nbsp;</td>

				<td><?php if ($category['Category']['parent_id']!=null):?>
					+
					<?php endif ?>
					<?php echo $this->Html->link($category['Category']['name'],'danh-muc'.$category['Category']['slug']); ?>
					&nbsp;
				</td>
				<td><?php echo h($category['Category']['slug']); ?>&nbsp;</td>
				<td><?php echo h($category['Category']['description']); ?>&nbsp;</td>
				<td><?php echo h($category['Category']['created']); ?>&nbsp;</td>
				<td class="actions">
					<a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'view', $category['Category']['id'])); ?>"><button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></button></a>
					<a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'edit', $category['Category']['id'])); ?>"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i></button></a>
					<?php echo $this->Form->postLink($this->Html->tag('i', '', ['class'=>'glyphicon glyphicon-remove']), array('action' => 'delete', $category['Category']['id']), array('escape'=>false,'class'=>'btn btn-danger btn-xs', 'confirm' => __('Are you sure you want to delete # %s?', $category['Category']['id']))); ?>
				</td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
