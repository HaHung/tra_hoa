
<div class="col-md-6">
	<div class="content-box-large">

		<div class="panel-body">
			<?php echo $this->Form->create('Category'); ?>
			<fieldset>
				<div>
					<h4><legend><?php echo __('Sửa danh mục sản phẩm'); ?></legend></h4>
					<?php echo $this->Form->input('id'); ?>
					<p>
					<div class="form-group">
						<label>Danh mục</label>
						<div class="input">
							<?php echo $this->Form->select('parent_id',$categories,array(
									'empty'=>'------Chọn danh mục------',
									'required' => false,
									'class' => 'form-control'
							)); ?>
						</div>
					</div>
					</p>
				</div>
				<div class="form-group">
					<label>Tên</label>
					<?php echo $this->Form->input('name', array('label' => false,
							'placeholder' => 'Tên danh mục',
							'class' => 'form-control'
					));
					?>
				</div>
				<div class="form-group">
					<label>Slug</label>
					<?php echo $this->Form->input('slug', array('label' => false,
							'placeholder' => 'slug',
							'class' => 'form-control'
					)); ?>

				</div>
				<div class="form-group">
					<label>Mô tả</label>
					<?php echo $this->Form->input('description', array('label' => false,
							'placeholder' => 'Mô tả',
							'class' => 'form-control'
					)); ?>

				</div>
			</fieldset>
			<div>
				<?php echo $this->Form->end(array(
						'label' => 'Save',
						'class' => 'btn btn-primary'
				)); ?>
			</div>

		</div>
	</div>
</div>

