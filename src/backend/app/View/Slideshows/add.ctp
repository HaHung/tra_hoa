<div class="slideshows form">
<?php echo $this->Form->create('Slideshow'); ?>
	<fieldset>
		<legend><?php echo __('Add Slideshow'); ?></legend>
	<?php
		echo $this->Form->input('image');
		echo $this->Form->input('title');
		echo $this->Form->input('status');
		echo $this->Form->input('url');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Slideshows'), array('action' => 'index')); ?></li>
	</ul>
</div>
