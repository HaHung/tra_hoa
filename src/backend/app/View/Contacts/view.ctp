

<h1><span class="label label-danger">Chi tiết liên hệ</span></h1>

        <div class="col-md-6 col-md-offset-3">

            <div class="well text-center">
                <p><small><?php echo $contact['Contact']['created']?></small></p>
                <form>
                    <input disabled type='hidden' name='csrfmiddlewaretoken' value='jirY00s8p1JmoEVdHSHBrH4rXryWR0jU' />
                    <div class="form-group">
                        <label>Họ tên khách hàng</label>
                        <input disabled class="form-control"  value="<?php echo $contact['Contact']['firstname'].' '.$contact['Contact']['lastname']; ?>" type="text" />
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input disabled class="form-control"  value="<?php echo $contact['Contact']['email']; ?>" type="email" />

                    </div>

                    <div class="form-group">
                        <label>Địa chỉ</label>
                        <input disabled class="form-control" value="<?php echo $contact['Contact']['address']; ?>" type="text" />

                    </div>

                    <div class="form-group">
                        <label>Số điện thoại</label>
                        <input disabled class="form-control" value="<?php echo $contact['Contact']['phone_number']; ?>" type="number" />

                    </div>

                    <div class="form-group">
                        <label>Nội dung</label>
                     <textarea disabled class="form-control" cols="40" rows="10"><?php echo $contact['Contact']['content']; ?>
                     </textarea>

                    </div>
                </form>

            </div>
        </div>


    </div>
</div>