
        <div class="content-box-large">

            <div class="panel-heading">
                <h1><span class="label label-danger">Liên hệ</span></h1>
            </div>

            <div class="panel-body">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                    <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('Họ Tên'); ?></th>
                        <th><?php echo $this->Paginator->sort('Email'); ?></th>
                        <th><?php echo $this->Paginator->sort('Địa chỉ'); ?></th>
                        <th><?php echo $this->Paginator->sort('Số điện thoại'); ?></th>
                        <th><?php echo $this->Paginator->sort('Nội dung'); ?></th>
                        <th><?php echo $this->Paginator->sort('Thời gian'); ?></th>
                        <th class="actions"><?php echo __('Hành động'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($contacts as $value): ?>
                        <tr class="odd gradeX">
                            <td> <?= $value['Contact']['firstname'].' '.$value['Contact']['lastname'] ?></td>
                            <td> <?= $value['Contact']['email'] ?>&nbsp;</td>
                            <td> <?= $value['Contact']['address'] ?>&nbsp;</td>
                            <td> <?= $value['Contact']['phone_number'] ?>&nbsp;</td>
                            <td> <?= $value['Contact']['content'] ?> </td>
                            <td> <?= $value['Contact']['created'] ?> </td>
                            <td class="actions">
                                <a href="<?php echo $this->Html->url(array('controller' => 'contacts', 'action' => 'view', $value['Contact']['id'])); ?>"><button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></button></a>
                                <?php echo $this->Form->postLink($this->Html->tag('i', '', ['class'=>'glyphicon glyphicon-remove']), array('action' => 'delete', $value['Contact']['id']), array('escape'=>false,'class'=>'btn btn-danger btn-xs', 'confirm' => __('Bạn có chắc là muốn xóa không?', $value['Contact']['id']))); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
                <ul class="pagination">

                    <li> <?php echo $this->Paginator->prev("<< Previous  "); ?></li>
                    <li> <?php echo $this->Paginator->numbers(); ?></li>
                    <li> <?php echo $this->Paginator->next("  Next >> ") ?></li>

                </ul>

                Trang: <?php echo $this->Paginator->counter(); ?>
            </div>
        </div>



