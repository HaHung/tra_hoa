<div class="content-box-large">
    <div class="panel-heading">
        <h1><span class="label label-danger">Thêm sản phẩm</span></h1>

        <div class="panel-options">
            <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
            <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <div id="rootwizard">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="container">
                        <ul class="nav nav-pills">
                            <li class="active"><a href="#tab1" data-toggle="tab">Trang 1</a></li>
                            <li><a href="#tab2" data-toggle="tab">Trang 2</a></li>
                            <li><a href="#tab3" data-toggle="tab">Trang 3</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">


                    <?php echo $this->Form->create('Flower', array(
                        'class' => 'form-horizontal',
                        'role' => 'form',
                        'inputDefaults' => array(
                            'class' => array('form-control'),
                        ))); ?>
                    <?php
                    echo $this->Form->input('category_id', array(
                        'label' => false,

                        'before' => '<label">',
                        'after' => '<i></i>ID</label>'));
                    echo $this->Form->input('title');
                    echo $this->Form->input('slug');
                    echo $this->Form->input('image');

                    echo $this->Form->textarea('info',array('class'=>'ckeditor'));

                    ?>
                </div>

                <div class="tab-pane active" id="tab2">
                    <?php
                    echo $this->Form->input('price');
                    echo $this->Form->input('sale_price');
                    echo $this->Form->input('sale_off');
                    ?>
                </div>

                <div class="tab-pane active" id="tab3">
                    <?php
                    echo $this->Form->checkbox('published');
                    echo $this->Form->end(__('Hoàn tất'));
                    ?>

                </div>

                <ul class="pager wizard">
                    <li class="previous first disabled" style="display:none;"><a href="javascript:void(0);">First</a></li>
                    <li class="previous disabled"><a href="javascript:void(0);">Previous</a></li>
                    <li class="next last" style="display:none;"><a href="javascript:void(0);">Last</a></li>
                    <li class="next"><a href="javascript:void(0);">Next</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>





