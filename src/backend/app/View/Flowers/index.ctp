<?php pr($this->request->data('[Flower][category_id]')); ?>
<div class="content-box-large">

    <div class="panel-heading">
        <h1><span class="label label-danger">Sản phẩm</span></h1>
    </div>

    <div class="panel-body">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Loại sản phẩm'); ?></th>
                <th><?php echo $this->Paginator->sort('Tên sản phẩm'); ?></th>
                <th><?php echo $this->Paginator->sort('Ảnh'); ?></th>
                <th><?php echo $this->Paginator->sort('Thông tin'); ?></th>
                <th><?php echo $this->Paginator->sort('Giá (VNĐ)'); ?></th>
                <th><?php echo $this->Paginator->sort('Giá bán buôn (VNĐ)'); ?></th>
                <th><?php echo $this->Paginator->sort('Giá khuyến mãi (VNĐ)'); ?></th>
                <th><?php echo $this->Paginator->sort('///'); ?></th>
                <th><?php echo $this->Paginator->sort('Ngày tạo'); ?></th>
                <th><?php echo $this->Paginator->sort('Ngày chỉnh sửa'); ?></th>
                <th class="actions"><?php echo __('Hành động'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($flowers as $value): ?>
                <tr class="odd gradeX">
                    <td> <?= $value['Flower']['category_id'] ?></td>
                    <td> <?= $value['Flower']['title'] ?></td>
                    <td> <img src="<?= $value['Flower']['image'] ?>" alt="img" width="100px" height="100px" /></td>
                    <td> <?= $value['Flower']['info'] ?></td>
                    <td> <?= $value['Flower']['price'] ?> </td>
                    <td> <?= $value['Flower']['sale_price'] ?> </td>
                    <td> <?= $value['Flower']['sale_off'] ?></td>
                    <td> <?= $value['Flower']['published'] ?></td>
                    <td> <?= $value['Flower']['created'] ?> </td>
                    <td> <?= $value['Flower']['modified'] ?> </td>
                    <td class="actions">
                        <a href="<?php echo $this->Html->url(array('controller' => 'Flowers', 'action' => 'view', $value['Flower']['id'])); ?>"><button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></button></a>
                        <a href="<?php echo $this->Html->url(array('controller' => 'Flowers', 'action' => 'edit', $value['Flower']['id'])); ?>"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i></button></a>
                        <?php echo $this->Form->postLink($this->Html->tag('i', '', ['class'=>'glyphicon glyphicon-remove']), array('action' => 'delete', $value['Flower']['id']), array('escape'=>false,'class'=>'btn btn-danger btn-xs', 'confirm' => __('Bạn có chắc là muốn xóa không?', $value['Flower']['id']))); ?>
                    </td>
                </tr>
            <?php endforeach; ?>






            </tbody>
        </table>

        <ul class="pagination">

            <li> <?php echo $this->Paginator->prev("<< Previous  "); ?></li>
            <li> <?php echo $this->Paginator->numbers(); ?></li>
            <li> <?php echo $this->Paginator->next("  Next >> ") ?></li>

        </ul>

        Trang: <?php echo $this->Paginator->counter(); ?>
    </div>
</div>



