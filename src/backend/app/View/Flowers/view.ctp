

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= $flower['Flower']['title'] ?></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center"><a href="<?= $flower['Flower']['image'] ?>"><img alt="User Pic" src="<?= $flower['Flower']['image'] ?>" class=" img-responsive"> </a> </div>

                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td><b>Loại sản phẩm:<b></td>
                                    <td><?= $flower['Flower']['category_id'] ?></td>
                                </tr>

                                <tr>
                                    <td><b>Giá (VNĐ)</b></td>
                                    <td><?= $flower['Flower']['price'] ?></td>
                                </tr>

                                <tr>
                                <tr>
                                    <td><b>Giá bán buôn (VNĐ)</b></td>
                                    <td><?= $flower['Flower']['sale_price'] ?></td>
                                </tr>
                                <tr>
                                    <td><b>Giá khuyến mãi (VNĐ)</b></td>
                                    <td><?= $flower['Flower']['sale_off'] ?></td>
                                </tr>
                                <tr>
                                    <td><b>///</b></td>
                                    <td><?= $flower['Flower']['published'] ?></td>
                                </tr>
                                <tr>
                                    <td><b>Ngày tạo</b></td>
                                    <td><?= $flower['Flower']['created'] ?></td>
                                </tr>
                                <tr>
                                    <td><b>Sửa lần cuối</b></td>
                                    <td><?= $flower['Flower']['modified'] ?></td>
                                </tr>
                                <tr>
                                    <td><b>Thông tin:</b></td>
                                    <td><?= $flower['Flower']['info'] ?></td>
                                </tr>

                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="<?php echo $this->Html->url(array('controller' => 'Flowers', 'action' => 'edit', $flower['Flower']['id'])); ?>"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i></button></a>
                    <?php echo $this->Form->postLink($this->Html->tag('i', '', ['class'=>'glyphicon glyphicon-remove']), array('action' => 'delete', $flower['Flower']['id']), array('escape'=>false,'class'=>'btn btn-danger btn-xs', 'confirm' => __('Bạn có chắc là muốn xóa không?', $flower['Flower']['id']))); ?>
                </div>

            </div>
