<!DOCTYPE html>
<html>
<head>
    <title>Quản trị | Admin</title>
    <meta charset="utf8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- icon
    <link rel="shortcut icon" type="image/gif" href="images/logo.gif" /> -->
<?php echo $this->Html->meta("/images/logo.gif", "/images/logo.gif", array('type' => 'icon')); ?>

<!-- Bootstrap
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<?php echo $this->Html->css([
        '/bootstrap/css/bootstrap.min',
        'styles',
        '/font-awesome/css/font-awesome',
        'https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css',
        'https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css',
        '/vendors/form-helpers/css/bootstrap-formhelpers.min.css',
        '/vendors/select/bootstrap-select.min.css',
        '/vendors/tags/css/bootstrap-tags.css',
        'forms.css',
        '/vendors/datatables/dataTables.bootstrap.css',
        '/vendors/bootstrap-datetimepicker/datetimepicker.css',
        'https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css'
]);

echo $this->fetch('styles');
?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?php echo $this->Html->script([
    'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js',
    'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js',
    'https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js',
    'table'
    ]); ?>
    <![endif]-->
</head>
<body>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="#"><!--<img src="images/logo.gif" alt="tts image" height="30px", width="30px"/>-->
                            <?php echo $this->Html->image("/images/logo.gif", array("height" => "30px", "width" => "30px")); ?>
                            Thảo Trà Shop</a></h1>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group form">
                            <input type="text" class="form-control" placeholder="Tìm kiếm...">
                            <span class="input-group-btn">
	                         <button class="btn btn-primary" type="button">Tìm kiếm</button>
	                       </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tài khoản <b
                                            class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a href="profile.html"><i class="fa fa-wheelchair"></i> Đổi mật khẩu</a></li>
                                    <li><a href="users/logout"><i class="fa fa-sign-out"></i> Đăng xuất</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <li class="current"><a href="/default/index"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>

                    <!-- Người dùng -->
                    <li class="submenu">
                        <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index')); ?>">
                            <i class="fa fa-group"></i>Người dùng
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li>
                                <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index')); ?>"><i
                                            class="fa fa-list"></i> Danh sách người dùng</a></li>
                            <li>
                                <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'add')); ?>"><i
                                            class="fa fa-plus"></i> Thêm người dùng</a></li>
                        </ul>
                    </li>

                    <!-- Sản phẩm -->
                    <li class="submenu">
                        <a href="#">
                            <i class="fa fa-product-hunt"></i>Sản phẩm
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li><a href="<?php echo $this->Html->url(array('controller'=>'flowers', 'action'=>'index')); ?>"><i class="fa fa-list"></i> Danh sách sản phẩm</a></li>
                            <li><a href="<?php echo $this->Html->url(array('controller'=>'flowers', 'action'=>'add')); ?>"><i class="fa fa-plus"></i> Thêm sản phẩm</a></li>
                        </ul>
                    </li>

                    <!--Danh muc sản phẩm -->
                    <li class="submenu">
                        <a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'index')); ?>">
                            <i class="fa fa-product-hunt"></i>DM sản phẩm
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li><a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'index')); ?>"><i class="fa fa-list"></i> DM sản phẩm</a></li>
                            <li><a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'add')); ?>"><i class="fa fa-plus"></i> Thêm DM sản phẩm</a></li>
                        </ul>
                    </li>
                    
                    <!-- Bài viết -->
                    <li class="submenu">
                        <a href="#">
                            <i class="fa fa-sticky-note-o"></i>Bài viết
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li>
                                <a href="<?php echo $this->Html->url(array('controller' => 'posts', 'action' => 'index')); ?>"><i
                                            class="fa fa-list"></i> Danh sách bài viết</a></li>
                            <li><a href="<?php echo $this->Html->url(array('controller' => 'posts', 'action' => 'add')); ?>"><i class="fa fa-plus"></i> Thêm bài viết</a></li>
                        </ul>
                    </li>

                    <!-- Đơn hàng -->
                    <li class="submenu">
                        <a href="#">
                            <i class="fa fa-shopping-bag"></i>Đơn hàng
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li><a href="#"><i class="fa fa-list"></i> Danh sách đơn hàng</a></li>
                        </ul>
                    </li>

                    <!-- SlideShow -->
                    <li class="submenu">
                        <a href="#">
                            <i class="fa fa-snowflake-o"></i>SlideShow
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li><a href="#"><i class="fa fa-list"></i> Danh sách SlideShow</a></li>
                            <li><a href="#"><i class="fa fa-plus"></i> Thêm SlideShow</a></li>
                        </ul>
                    </li>

                    <!-- Liên hệ -->
                    <li class="submenu">
                        <a href="#">
                            <i class="fa fa-phone"></i>Liên hệ
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li>
                                <a href="<?php echo $this->Html->url(array('controller' => 'contacts', 'action' => 'index')); ?>"><i
                                            class="fa fa-list"></i> Danh sách liên hệ</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
        <div class="col-md-10">
            <!-- content -->
            <?php echo $this->fetch('content')?>
        </div>
    </div>
</div>

<footer>
    <div class="container">

        <div class="copy text-center">
            Copyright 2016 <a href='www.facebook.com'>TTS Corporation.</a>
            <p>Tập đoàn Thảo Trà Shop
                Giấy CNĐKDN: 25082016 - Ngày cấp: 06/12/2016
                Cơ quan cấp: Hackademic Hanoi.
                Địa chỉ đăng ký kinh doanh: Tầng 4, toà nhà ACCI, 210 Lê Trọng Tấn, Thanh Xuân, Hà Nội
                Số điện thoại: 0968778965</p>
        </div>

    </div>
</footer>

<?php echo $this->Html->script([
        'https://code.jquery.com/jquery.js',
        'https://code.jquery.com/ui/1.10.3/jquery-ui.js'
]); ?>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<?php echo $this->Html->script([
        '/bootstrap/js/bootstrap.min.js',
        '/bootstrap/js/bootstrap.min.js',
        '/vendors/datatables/js/jquery.dataTables.min.js',
        '/vendors/datatables/dataTables.bootstrap.js',

        '/vendors/form-helpers/js/bootstrap-formhelpers.min.js',
        '/vendors/select/bootstrap-select.min.js',
        '/vendors/tags/js/bootstrap-tags.min.js',
        '/vendors/mask/jquery.maskedinput.min.js',
        '/vendors/moment/moment.min.js',
        '/vendors/wizard/jquery.bootstrap.wizard.min.js',
        '/vendors/ckeditor/ckeditor',
        '/vendors/ckeditor/adapters/jquery',
        '/vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.js',
        'https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js',
        //'tables.js',
        'forms.js',
        'custom.js',
        'editors',
        'ckeditor/ckeditor'
]);

echo $this->fetch('scripts');
?>
</body>
</html>