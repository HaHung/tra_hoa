<!DOCTYPE html>
<html>
<head>
    <title>Admin login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo $this->Html->meta("/images/logo.gif", "/images/logo.gif", array('type' => 'icon')); ?>
    <!-- Bootstrap -->
    <?php echo $this->Html->css([
        '/bootstrap/css/bootstrap.min',
        '/font-awesome/css/font-awesome',
        'styles'
    ]); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <?php echo $this->Html->script([
    'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js',
    'https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js',
    ]); ?>

</head>
<body class="login-bg">
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="index.html">Admin login</a></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content container">
    <?php echo $this->fetch('content'); ?>
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php echo $this->Html->script([
        'https://code.jquery.com/jquery.js',
        '/bootstrap/js/bootstrap.min.js',
        'custom'
]); ?>
</body>
</html>