<?php
App::uses('AppController', 'Controller');
/**
 * Flowers Controller
 *
 * @property Flower $Flower
 * @property PaginatorComponent $Paginator
 */
class FlowersController extends AppController {
    public $layout='admin';
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->paginate=array(
		    'limit' => '5',
        );
		$flower=$this->paginate("Flower");
		$this->set('flowers', $flower);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Flower->exists($id)) {
			throw new NotFoundException(__('Invalid flower'));
		}
		$options = array('conditions' => array('Flower.' . $this->Flower->primaryKey => $id));
		$this->set('flower', $this->Flower->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
        if ($this->request->is('post')) {
            $this->Flower->create();
            if ($this->Flower->save($this->request->data)) {
                $this->Flash->success(__('The flower has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The flower could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Flower->Category->find('list');
        $this->set(compact('categories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Flower->exists($id)) {
			throw new NotFoundException(__('Invalid flower'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Flower->save($this->request->data)) {
				$this->Flash->success(__('The flower has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The flower could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Flower.' . $this->Flower->primaryKey => $id));
			$this->request->data = $this->Flower->find('first', $options);
		}
		$categories = $this->Flower->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Flower->id = $id;
		if (!$this->Flower->exists()) {
			throw new NotFoundException(__('Invalid flower'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Flower->delete()) {
			$this->Flash->success(__('The flower has been deleted.'));
		} else {
			$this->Flash->error(__('The flower could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
