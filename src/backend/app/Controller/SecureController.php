<?php
/**
 * Created by PhpStorm.
 * Secure: ntdha
 * Date: 12/12/2016
 * Time: 7:16 PM
 */

class SecureController extends AppController {
    public function login(){
        $this->layout=null;
        if($this->request->is('post')){
            if($this->Auth->login()){
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid Username or password'));
        }
    }

    public function logout(){
        return $this->redirect($this->Auth->logout());
    }
}