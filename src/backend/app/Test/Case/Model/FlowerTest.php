<?php
App::uses('Flower', 'Model');

/**
 * Flower Test Case
 */
class FlowerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.flower',
		'app.category',
		'app.comment',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Flower = ClassRegistry::init('Flower');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Flower);

		parent::tearDown();
	}

}
